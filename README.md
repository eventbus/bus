![Eventbus.png](https://bitbucket.org/repo/dagkXBp/images/2765864500-Eventbus.png)

[![Release](https://jitpack.io/v/org.bitbucket.eventbus/core.svg)](https://jitpack.io/#org.bitbucket.eventbus/core)
[![codecov](https://codecov.io/bb/eventbus/core/branch/master/graph/badge.svg)](https://codecov.io/bb/eventbus/core)
[![Android Arsenal](https://img.shields.io/badge/Android%20Arsenal-EventBus-brightgreen.svg?style=flat)](https://android-arsenal.com/details/1/6698)

# EventBus is #

* Convenient and easy to use:
    * No need to create event classes
    * Opportunity to execute subscriber on background thread
* Fast
* Tiny (~11K jar)
* Well-tested (more than 45 tests)

### How to use? ###

* Get event bus instance
```java
    EventBus bus = EventBus.getInstance();
```

* Ans make a simple subscribe on the event  
```java
    bus.on("MyEvent", data -> System.out.println("Event delivered"));
```

* Or subscribe on the event on specified channel like this
```java
    bus.on("MyEvent", data -> System.out.println("Event delivered"));
```

* Or like this
```java
    bus.once("someEvent", this::someData);
```

* Or make a chain of events
```java
    bus.on("event1", this::process)
    .on("channel", "event1", this::process);
```

* Subscribe once
```java
    bus.once("event1", this::process);
```

* Subscribe on specified thread
```java
    bus.on("event1", this::process, Mode.BACKGROUND);
```

* Or combine all together
```java
    bus.on("globalEvent", this::globalEvent)
    .on("channel", "event", this::channelEventOnCurrentThread)
    .on("channelNew", "eventNew", data -> System.out.println("Event delivered"), Mode.BACKGROUND)
    .once("eventOnce", new Consumer() {
      @Override
      public void accept(Object data) {
        System.out.println(data);
      }
    }), Task.Mode.BACKGROUND)
```

* And send some the event
```java
    bus.post("MyEvent", "MyData")
    .post("userEvent", new User())
    .post("intEvent", 123)
    .post("anyEvent", new ArrayList<String>());
```


### Plugin system ###
MQTT Support

### Comming soon ###
  - Android support
  - Send distributed events throught several services via HTTP/WS
* Logs & Monitoring



For more information see [JavaDoc](https://eventbus.bitbucket.io/)


### Dependencies ###

##### **Step 1. Add the JitPack repository to your build file** #####

Add it in your root build.gradle at the end of repositories:
```gradle
allprojects {
	repositories {
		...
		maven { url 'https://jitpack.io' }
	}
}
```

##### **Step 2. Add the dependency** #####

```gradle
dependencies {
	compile 'org.bitbucket.eventbus:bus:v1.2'
}
```



### Licence ###

    Copyright (C) 2018
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see https://www.gnu.org/licenses/