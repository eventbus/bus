package com.bitbucket.eventbus;

import com.bitbucket.eventbus.core.enums.Mode;
import com.bitbucket.eventbus.core.interfaces.Bus;
import com.bitbucket.eventbus.core.interfaces.Consumer;
import com.bitbucket.eventbus.core.interfaces.Pluggable;
import com.bitbucket.eventbus.executors.Executor;
import com.bitbucket.eventbus.models.Channel;
import com.bitbucket.eventbus.utils.PluginManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents the event bus.
 */
public class EventBus implements Bus {

  /**
   * If channel name is not specified, events are posted to this channel
   */
  public static final String DEFAULT_CHANNEL = "global";

  private static EventBus instance;
  private final Executor executor;
  private final PluginManager pluginManager;

  // Keeps all channels
  // key - channel name; value - Channel
  private Map<String, Channel> channels;

  /**
   * Call this method to get event bus instance. Event bus is created as singleton.
   *
   * @return Event bus instance.
   */
  public static synchronized EventBus getInstance() {
    if (instance == null) {
      init();
    }
    return instance;
  }

  /**
   * Helper method for creating new event bus instance.
   * Call this method if event bus must be recreated.
   * For example in case the internal thread pool has been shut down.
   */
  public static void init() {
    instance = new EventBus();
  }

  // Constructor is private. Call getInstance() to get event bus instance.
  private EventBus() {
    channels = new HashMap<>();
    pluginManager = new PluginManager(this);
    executor = new Executor();
  }

  /**
   * Gets event bus executor instance.
   *
   * @return Event bus executor.
   */
  public Executor getExecutor() {
    return executor;
  }

  /**
   * Gets channels.
   *
   * @return All channels currently present in event bus.
   */
  public Map<String, Channel> getChannels() {
    return channels;
  }


  public PluginManager getPluginManager(){
    return pluginManager;
  }

  // Subscribe to the event on specified channel. Subscriber is executed in specified mode.
  // Subscriber is removed from the event bus after the event is posted limit times.
  private void subscribe(String channel, String event, Consumer task, int limit, Mode mode) {
    if ( channel != null && !channel.equals("") && event != null && !event.equals("") && task != null ) {

      if ( !channels.containsKey(channel) ) {
        channels.put(channel, new Channel());
      }
      Channel channelModel = channels.get(channel);
      channelModel.add(event, task, limit, mode);

      if (pluginManager.hasChannel(channel)){
        pluginManager.subscribe(channel, event, mode);
      }
    }
  }

  // Post event on specified channel
  private void publish(String channel, String event, Object data){
    if ( channel != null && !channel.equals("") && event != null && !event.equals("") && data != null ) {

      // if pluggins has channel
      if (pluginManager.hasChannel(channel)){
        pluginManager.publish(channel, event, data);
      } else {
        if ( channels.containsKey(channel) ) {
          Channel channelModel = channels.get(channel);
          channelModel.post(event, data);
        }
      }
    }
  };

  @Override
  public Bus on(String event, Consumer task) {
    subscribe(DEFAULT_CHANNEL, event, task, -1, Mode.CURRENT);
    return this;
  }

  @Override
  public Bus on(String channel, String event, Consumer task) {
    subscribe(channel, event, task, -1, Mode.CURRENT);
    return this;
  }

  @Override
  public Bus on(String channel, String event, Consumer task, Mode mode) {
    subscribe(channel, event, task, -1, mode);
    return this;
  }

  @Override
  public Bus once(String event, Consumer task) {
    subscribe(DEFAULT_CHANNEL, event, task, 1, Mode.CURRENT);
    return this;
  }

  @Override
  public Bus once(String channel, String event, Consumer task) {
    subscribe(channel, event, task, 1, Mode.CURRENT);
    return this;
  }

  @Override
  public Bus once(String channel, String event, Consumer task, Mode mode) {
    subscribe(channel, event, task, 1, mode);
    return this;
  }

  @Override
  public Bus many(String event, Integer count, Consumer task) {
    if ( count != null && count > 0 ) {
      subscribe(DEFAULT_CHANNEL, event, task, count, Mode.CURRENT);
    }
    return this;
  }

  @Override
  public Bus many(String channel, String event, Integer count, Consumer task) {
    if ( count != null && count > 0 ) {
      subscribe(channel, event, task, count, Mode.CURRENT);
    }
    return this;
  }

  @Override
  public Bus many(String channel, String event, Integer count, Consumer task, Mode mode) {
    if ( count != null && count > 0 ) {
      subscribe(channel, event, task, count, mode);
    }
    return this;
  }

  @Override
  public Bus post(String event, Object object) {
    publish(DEFAULT_CHANNEL, event, object);
    return this;
  }

  @Override
  public Bus post(String channel, String event, Object object) {
    publish(channel, event, object);
    return this;
  }

  @Override
  public Bus unsubscribe(String channel) {
    channels.remove(channel);
    return this;
  }

  @Override
  public Bus withPlugin(Pluggable pluggable) {
    pluginManager.add(pluggable);
    return this;
  }

}
