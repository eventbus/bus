package com.bitbucket.eventbus.executors;

import com.bitbucket.eventbus.core.interfaces.Executable;
import com.bitbucket.eventbus.models.Task;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Executor for subscriber tasks with background mode.
 * Executes tasks on the background thread inside thread pool.
 */
public class BackgroundExecutor implements Executable {

  // Executes tasks
  private final ExecutorService pool;

  /**
   * Instantiates a new background executor.
   */
  BackgroundExecutor() {
    this.pool = Executors.newWorkStealingPool();
  }

  /**
   * Gets thread pool that executes subscriber tasks with background mode specified.
   *
   * @return Task execution thread pool.
   */
  public ExecutorService getPool() {
    return pool;
  }

  @Override
  public void execute(Runnable runnable) {
    // Initiate task execution by submitting task COPY into the thread pool,
    // so that every task execution will be performed with the data posted with the corresponding event
    // in case of the event with the same name posted multiple times with different data.
    // Current thread is NOT blocked.
    if (runnable instanceof Task){
      pool.submit( ((Task) runnable).clone() );
    }
  }
}
