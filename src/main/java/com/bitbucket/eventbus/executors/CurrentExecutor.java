package com.bitbucket.eventbus.executors;

import com.bitbucket.eventbus.core.interfaces.Executable;

/**
 * Executor for subscriber tasks with current mode.
 * Executes tasks on the same thread from which the event was posted.
 */
public class CurrentExecutor implements Executable {

  @Override
  public void execute(Runnable task) {
    // Execute task by run() method invocation.
    // Task execution blocks current thread.
    // Task is not copied as in background executor,
    // because in current mode executor there is no simultaneous task execution,
    // so no possibility of losing data posted with the event.
    task.run();
  }
}
