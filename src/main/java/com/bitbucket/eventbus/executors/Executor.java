package com.bitbucket.eventbus.executors;

import com.bitbucket.eventbus.core.interfaces.Executable;
import com.bitbucket.eventbus.models.Task;

/**
 * The {@code Executor} keeps specific executor instances for each execution modes
 * and executes subscriber tasks in the appropriate executor depending on execution mode specified inside task.
 */
public class Executor implements Executable {
  // Executor for the background mode
  private final BackgroundExecutor backgroundExecutor;

  // Executor for the current mode
  private final CurrentExecutor currentExecutor;

  /**
   * Instantiates a new Executor.
   */
  public Executor() {
    this.backgroundExecutor = new BackgroundExecutor();
    this.currentExecutor = new CurrentExecutor();
  }

  /**
   * Gets executor for the background mode.
   *
   * @return Background mode executor.
   */
  public BackgroundExecutor getBackgroundExecutor() {
    return backgroundExecutor;
  }

  @Override
  public void execute(Runnable runnable) {
    if (runnable instanceof Task){
      Task task = (Task) runnable;
      // Check and remove task if needed
      task.check();

      // Execute task in main or background executor depending on execution mode specified inside task
      switch ( task.getMode() ){
        case CURRENT:
          currentExecutor.execute(task);
          break;
        case BACKGROUND:
          backgroundExecutor.execute(task);
          break;
      }
    }
  }
}
