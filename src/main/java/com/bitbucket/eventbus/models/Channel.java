package com.bitbucket.eventbus.models;

import com.bitbucket.eventbus.EventBus;
import com.bitbucket.eventbus.core.enums.Mode;
import com.bitbucket.eventbus.core.interfaces.Consumer;
import com.bitbucket.eventbus.core.interfaces.Removable;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Keeps events of one channel and tasks subscribed to these events.
 */
public class Channel implements Removable {

  // Keeps all channel's events and tasks
  // key - event name; value - list of corresponding tasks
  private Map<String, CopyOnWriteArrayList<Task>> events;

  /**
   * Instantiates a new Channel.
   */
  public Channel() {
    events = new HashMap<>();
  }

  /**
   * Gets events.
   *
   * @return All events currently present in the channel.
   */
  public Map<String, CopyOnWriteArrayList<Task>> getEvents() {
    return events;
  }

  /**
   * Add subscriber to the event in this channel.
   *
   * @param event    Name of the event to subscribe to
   * @param consumer Subscriber
   * @param limit    Number of times the subscriber is executed after event is posted
   * @param mode     Subscriber execution mode
   */
  public void add(String event, Consumer consumer, long limit, Mode mode) {
    if ( consumer != null && event != null ) {

      Task task = new Task( limit, consumer, mode, this, event );

      if ( !events.containsKey( event ) ) {
        events.put( event, new CopyOnWriteArrayList<Task>() );
      }

      CopyOnWriteArrayList<Task> list = events.get( event );
      list.add( task );
    }
  }

  @Override
  public void remove(Runnable runnable) {
    if (runnable instanceof Task){
      Task task = (Task) runnable;
      if ( hasTasks(task.getEvent()) ) {
        events.get(task.getEvent()).remove(task);
      }
    }
  }

  /**
   * Post event to this channel.
   *
   * @param event  Name of the posted event
   * @param object Data posted with the event
   */
  public void post(String event, Object object) {
    if ( hasTasks(event) ) {
      for (Task task : events.get(event)) {
        task.setData(object);
        EventBus.getInstance().getExecutor().execute(task);
      }
    }
  }

  // Return true if channel contains event with the specified name and this event has subscriber tasks
  private boolean hasTasks(String event){
    return events.get(event) != null && events.get(event).size() > 0;
  }

}
