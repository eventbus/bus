package com.bitbucket.eventbus.models;


import com.bitbucket.eventbus.core.enums.Mode;
import com.bitbucket.eventbus.core.interfaces.Consumer;

/**
 * Subscriber task. The {@code Task} class is a helper wrapper around subscriber ({@code Consumer}).
 * Keeps subscriber instance and its parameters of execution,
 * such as subscriber execution mode, number of times the task must be executed,
 * number of times the task has been already executed etc.
 * Tasks in background mode are submitted to the thread pool,
 * so the {@code Task} class implements {@code java.lang.Runnable} interface.
 * Subscriber execution is invoked inside the {@code run()} method.
 * Before submitting to the thread pool {@code Task} must be cloned,
 * so that every task execution is performed with the exact data posted with the corresponding event
 * in case the event with the same name is posted again with different data
 * before previously posted events have finished processing.
 */
public class Task implements Runnable, Cloneable {

  // Channel on which event the task is subscribed to
  private final Channel channel;

  // Name of the event the task is subscribed to
  private final String event;

  // Number of times the task is executed before removal.
  // Task is unlimited if limit = -1
  private long limit;

  // Data posted with the event
  private Object data;

  // Subscriber instance
  private Consumer consumer;

  // Subscriber execution mode
  private Mode mode;

  // Number of times the task has been already executed
  private long counter;

  /**
   * Instantiates a new Task.
   *
   * @param limit    Number of times the task is executed before removal. -1 if task is unlimited
   * @param consumer Subscriber
   * @param mode     Subscriber execution mode
   * @param channel  Channel on which event the task is subscribed to
   * @param event    Name of the event the task is subscribed to
   */
  Task(long limit, Consumer consumer, Mode mode, Channel channel, String event) {
    this.limit = limit;
    this.consumer = consumer;
    this.counter = 0;
    this.mode = mode;
    this.event = event;
    this.channel = channel;
  }

  /**
   * Gets name of the event the task is subscribed to.
   *
   * @return Event name.
   */
  public String getEvent() {
    return event;
  }

  /**
   * Gets subscriber execution mode.
   *
   * @return Subscriber execution mode.
   */
  public Mode getMode() {
    return mode;
  }

  /**
   * Save data posted with the event.
   *
   * @param data Data posted with the event
   */
  void setData(Object data) {
    this.data = data;
  }

  /**
   * Increment number of times the task has been executed and remove task from the channel
   * if task is not unlimited and execution counter exceeds limit.
   * Method must be called before task execution.
   */
  public void check() {
    counter++;
    if ( limit != -1 && counter >= limit ){
      channel.remove(this);
    }
  }

  @Override
  public void run() {
    // Consumer.accept() is called inside try-catch block
    // to prevent application that uses event bus from crash
    // in case of the exception thrown inside the subscriber method
    // (for example, division by zero).
    try {
      consumer.accept( data );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public Task clone() {
    Task copy = null;

    try {
      copy = (Task) super.clone();
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }

    return copy;
  }
}
