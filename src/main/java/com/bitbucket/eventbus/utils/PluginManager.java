package com.bitbucket.eventbus.utils;

import com.bitbucket.eventbus.EventBus;
import com.bitbucket.eventbus.core.enums.Mode;
import com.bitbucket.eventbus.core.interfaces.Pluggable;
import com.bitbucket.eventbus.models.Channel;

import java.util.ArrayList;

public class PluginManager {
  private final EventBus eventBus;
  private ArrayList<Pluggable> plugins;
  private ArrayList<String> channels;

  public PluginManager(EventBus eventBus) {
    this.eventBus = eventBus;
    this.plugins  = new ArrayList<>();
    this.channels = new ArrayList<>();
  }

  public void add(Pluggable pluggable){
    if ( !channels.contains(pluggable.getChannel()) ){
      channels.add(pluggable.getChannel());
    }

    plugins.add(pluggable);
    pluggable.start();
  }

  public void clear(){
    if ( plugins.size() > 0 ){
      for (Pluggable plugin: plugins) {
        plugin.stop();
      }
    }
    this.plugins  = new ArrayList<>();
    this.channels = new ArrayList<>();
  }

  public void subscribe(String channel, String event, Mode mode) {
    if ( plugins.size() > 0 ){
      for (Pluggable plugin: plugins) {
        if (plugin.getChannel().equals(channel)){
          plugin.subscribe(eventBus, channel, event, mode);
        }
      }
    }
  }
  public void publish(String channel, String event, Object data) {
    if ( plugins.size() > 0 ){
      for (Pluggable plugin: plugins) {

        if (event.startsWith("_")) {
          if (eventBus.getChannels().containsKey(channel)) {
            Channel channelModel = eventBus.getChannels().get(channel);
            channelModel.post(event.replace("_", ""), data);
          }
        } else {
          plugin.publish(eventBus, channel, event, data);
        }

      }
    }
  }

  public Boolean hasChannel(String channel){
    return channels.contains(channel);
  }
}
