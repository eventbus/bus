import com.bitbucket.eventbus.core.enums.Mode;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


// Test subscribers execution in background threads
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BackgroundTest {

  private TestHelper helper;

  @Before
  public void init() {
    helper = new TestHelper();
  }

  @Test
  public void basicChannelDeliveryOnBackground() {
    // Test BASIC_EVENT delivery on BASIC_CHANNEL in background mode.

    helper.getBus().on( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT, helper::onBasicEvent, Mode.BACKGROUND );
    helper.postBasicChannelBasicEvent( Constants.SEND_EVENT_COUNT );
    helper.checkDeliveredSlow();
  }

  @Test
  public void multipleEventDeliveryBackground() {
    // Check if multiple events are delivered in background mode

    helper.subscribeReceiverIncrementCounterBasicChannelBackground( TestHelper.ReceiverNumber.FIRST );
    helper.postSenderBasicChannelBasicEvent( Constants.EVENT_QUANTITY );
    helper.checkCounterSlow( Constants.EVENT_QUANTITY * Constants.FIRST_COUNTER_INCREMENT );
  }

  @Test
  public void multipleEventWithDataDeliveryBackground() {
    // Check if multiple events with data are delivered in background mode.
    // Send EVENT_WITH_DATA_QUANTITY events with data in background mode.
    // On every event increment Receiver's counter by the value posted with the event.
    // Check if the final counter value equals to the total sum of posted data values.

    helper.verifyMultipleEventWithDataDelivery( true );
  }

  @Test
  public void multipleSubscriberBackground() {
    // Test that event is delivered to several subscribers in background mode.
    // Subscribe SUBSCRIBE_BACKGROUND_COUNT subscribers.
    // Send 1 event.
    // Check if receiver counter has been incremented for SUBSCRIBE_BACKGROUND_COUNT times.

    helper.verifyMultipleSubscriberBackground( false );
  }

  @Test
  public void multipleSubscriberMainAndBackground() {
    // Test that event is delivered to several subscribers in main and background mode.
    // Subscribe SUBSCRIBE_BACKGROUND_COUNT subscribers in background mode and SUBSCRIBE_MAIN_COUNT subscribers in main mode.
    // Send 1 event.
    // Check if receiver counter has been incremented for SUBSCRIBE_BACKGROUND_COUNT + SUBSCRIBE_MAIN_COUNT times.

    helper.verifyMultipleSubscriberBackground( true );
  }

  @Test
  public void onceBackground() {
    // Test EventBus.once() with background mode

    helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).subscribeIncrementCounterOnceBackground( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT );
    helper.postAndCheckOnceAndManySlow( true );
  }

  @Test
  public void manyBackground() {
    // Test EventBus.many() with background mode

    helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).subscribeIncrementCounterManyBackground( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT, Constants.SUBSCRIBER_EVENT_COUNT );
    helper.postAndCheckOnceAndManySlow( false );
  }
}
