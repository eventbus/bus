import com.bitbucket.eventbus.EventBus;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;


// Test EventBus.on() and post() methods with default channel only
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BasicTest {

  private TestHelper helper;

  @Before
  public void init() {
    helper = new TestHelper();
  }

  @Test
  public void basicEventDeliveryMethodReference() {
    // Test BASIC_EVENT delivery.
    // Subscription method is provided via method reference.

    // Tell EventBus what to do on event with name BASIC_EVENT.
    // EventBus must call onBasicEvent() method.
    helper.subscribeBasicEvent();

    // Post BASIC_EVENT. EventBus calls onBasicEvent method
    helper.postBasicEvent();

    // Check if onBasicEvent() method has been called (BASIC_EVENT delivered)
    helper.checkDelivered();
  }

  @Test
  public void basicEventDeliveryLambda() {
    // Test BASIC_EVENT delivery.
    // Subscription method is provided via lambda.

    helper.getBus().on( Constants.BASIC_EVENT, object -> helper.setEventDelivered() );
    helper.postBasicEvent();
    helper.checkDelivered();
  }

  @Test
  public void unsubscribedEvent() {
    // Check if event is not delivered if there are no subscribers

    helper.postBasicEvent();
    helper.checkNotDelivered();
  }

  @Test
  public void nullEvent() {
    // Test subscription with null event.
    // Must be no exceptions and no event delivery.

    helper.getBus().on(null, helper::onBasicEvent );
    helper.getBus().post( null, Constants.STRING_DATA );
    helper.checkNotDelivered();
  }

  @Test
  public void emptyStringEvent() {
    // Test subscription with empty string event.
    // Must be no exceptions and no event delivery

    helper.getBus().on( Constants.EMPTY_STRING, helper::onBasicEvent );
    helper.getBus().post( Constants.EMPTY_STRING, Constants.STRING_DATA );
    helper.checkNotDelivered();
  }

  @Test
  public void nullSubscriber() {
    // Test subscription with null subscription method.
    // Must be no exceptions and no event delivery

    helper.getBus().on( Constants.BASIC_EVENT, null );
    helper.postBasicEvent();
    helper.checkNotDelivered();
  }

  @Test
  public void nullDataEvent() {
    // Send event with null data.
    // Must be no exceptions and no event delivery

    helper.subscribeBasicEvent();
    helper.getBus().post( Constants.BASIC_EVENT, null );
    helper.checkNotDelivered();
  }

  @Test
  public void stringDataEvent() {
    // Send event with String data.
    // Data must be delivered.

    helper.getBus().on( Constants.DATA_EVENT, helper::onStringEvent );
    helper.getBus().post( Constants.DATA_EVENT, Constants.STRING_DATA );
    helper.checkDelivered();
    assertEquals( Constants.STRING_DATA, helper.getDeliveredString() );
  }

  @Test
  public void intDataEvent() {
    // Send event with Integer data.
    // Data must be delivered.

    helper.getBus().on( Constants.DATA_EVENT, helper::onIntegerEvent );
    helper.getBus().post( Constants.DATA_EVENT, Constants.INTEGER_DATA );
    helper.checkDelivered();
    assertEquals( Constants.INTEGER_DATA, helper.getDeliveredInteger() );
  }

  @Test
  public void senderReceiverEventDelivery() {
    // Test BASIC_EVENT delivery between Sender and Receiver

    // Send event before receiver subscribes. Event must NOT be delivered
    helper.postSenderBasicEvent( 1 );
    helper.checkReceiverNotDelivered( TestHelper.ReceiverNumber.FIRST );

    // Send event after receiver subscribes. Event MUST be delivered
    helper.subscribeReceiverBasicEvent( TestHelper.ReceiverNumber.FIRST );
    helper.postSenderBasicEvent( 1 );
    helper.checkReceiverDelivered( TestHelper.ReceiverNumber.FIRST );
  }

  @Test
  public void eventPostDurationMeasure() {
    // Benchmark.
    // Subscribe to some event and post EVENT_QUANTITY events of this type.
    // Measure overall execution time and calculate average event handling time.

    helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).subscribeEmpty( Constants.EMPTY_EVENT );

    for (int i = 0; i < Constants.EVENT_QUANTITY; i++) {
      helper.getSender().send( Constants.EMPTY_EVENT, Constants.STRING_DATA );
    }
  }

  @Test
  public void eventPostDurationMeasureGreenRobot() {
    // GreenRobot EventBus benchmark.
    // Subscribe to some event and post EVENT_QUANTITY events of this type.
    // Measure overall execution time and calculate average event handling time.

    SenderGreenRobot senderGreenRobot = new SenderGreenRobot();
    ReceiverGreenRobot receiverGreenRobot = new ReceiverGreenRobot();
    receiverGreenRobot.register();

    for (int i = 0; i < Constants.EVENT_QUANTITY; i++) {
      senderGreenRobot.sendEmpty();
    }

    receiverGreenRobot.unregister();
  }

  @Test
  public void multipleEventDelivery() {
    // Check if multiple events are delivered

    helper.subscribeReceiverIncrementCounter( TestHelper.ReceiverNumber.FIRST );
    helper.postSenderBasicEvent( Constants.EVENT_QUANTITY );
    helper.checkCounter( Constants.EVENT_QUANTITY * Constants.FIRST_COUNTER_INCREMENT );
  }

  @Test
  public void multipleEventWithDataDelivery() {
    // Check if multiple events with data are delivered.
    // Send EVENT_WITH_DATA_QUANTITY events with data.
    // On every event increment Receiver's counter by the value posted with the event.
    // Check if the final counter value equals to the total sum of posted data values.

    helper.verifyMultipleEventWithDataDelivery( false );
  }

  @Test
  public void multipleReceiverDelivery() {
    // Check if BASIC_EVENT is delivered to multiple receivers

    helper.subscribeReceiverBasicEvent( TestHelper.ReceiverNumber.FIRST );
    helper.subscribeReceiverBasicEvent( TestHelper.ReceiverNumber.SECOND );
    helper.postSenderBasicEvent( 1 );
    helper.checkReceiverDelivered( TestHelper.ReceiverNumber.FIRST );
    helper.checkReceiverDelivered( TestHelper.ReceiverNumber.SECOND );
  }

  @Test
  public void unsubscribeDefaultChannel() {
    // Test EventBus.unsubscribe() method.
    // Subscribe to BASIC_EVENT on default channel.
    // Unsubscribe from default channel.
    // Check if BASIC_EVENT is not delivered.

    helper.subscribeBasicEvent();
    helper.getBus().unsubscribe( EventBus.DEFAULT_CHANNEL );
    helper.postBasicEvent();
    helper.checkNotDelivered();
  }
}
