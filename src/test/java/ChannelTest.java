import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.assertEquals;


// Test EventBus.on() and post() methods with non-default channel
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ChannelTest {

  private TestHelper helper;

  @Before
  public void init() {
    helper = new TestHelper();
  }

  @Test
  public void basicChannelDelivery() {
    // Test BASIC_EVENT delivery on BASIC_CHANNEL.

    // Subscribe on BASIC_EVENT event sent to BASIC_CHANNEL channel
    helper.getBus().on( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT, helper::onBasicEvent );

    // Post BASIC_EVENT to BASIC_CHANNEL. EventBus calls onBasicEvent method
    helper.postBasicChannelBasicEvent( 1 );

    // Check if onBasicEvent() method has been called (BASIC_EVENT delivered)
    helper.checkDelivered();
  }

  @Test
  public void nullChannel() {
    // Test subscription with null channel.
    // Must be no exceptions and no event delivery

    helper.getBus().on(null, Constants.BASIC_EVENT, helper::onBasicEvent );
    helper.getBus().post( null, Constants.BASIC_EVENT, Constants.STRING_DATA );
    helper.checkNotDelivered();
  }

  @Test
  public void emptyStringChannel() {
    // Test subscription with empty string channel.
    // Must be no exceptions and no event delivery

    helper.getBus().on( Constants.EMPTY_STRING, Constants.BASIC_EVENT, helper::onBasicEvent );
    helper.getBus().post( Constants.EMPTY_STRING, Constants.BASIC_EVENT, Constants.STRING_DATA );
    helper.checkNotDelivered();
  }

  @Test
  public void senderReceiverChannelDelivery() {
    // Test BASIC_EVENT delivery on BASIC_CHANNEL between Sender and Receiver

    // Send event before receiver subscribes. Event must NOT be delivered
    helper.postSenderBasicChannelBasicEvent( 1 );
    helper.checkReceiverNotDelivered( TestHelper.ReceiverNumber.FIRST );

    // Send event after receiver subscribes. Event MUST be delivered
    helper.subscribeReceiverBasicChannelBasicEvent( TestHelper.ReceiverNumber.FIRST );
    helper.postSenderBasicChannelBasicEvent( 1 );
    helper.checkReceiverDelivered( TestHelper.ReceiverNumber.FIRST );
  }

  @Test
  public void unsubscribeChannel() {
    // Test EventBus.unsubscribe() method
    // Subscribe to BASIC_CHANNEL.
    // Unsubscribe from BASIC_CHANNEL.
    // Check if event is not delivered.

    // Send event after receiver subscribes. Event MUST be delivered
    helper.subscribeReceiverBasicChannelBasicEvent( TestHelper.ReceiverNumber.FIRST );
    helper.postSenderBasicChannelBasicEvent( 1 );
    helper.checkReceiverDelivered( TestHelper.ReceiverNumber.FIRST );

    // Reset event delivered flag
    helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).setEventDelivered( false );

    // Send event after receiver unsubscribes. Event must NOT be delivered
    helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).unsubscribe( Constants.BASIC_CHANNEL );
    helper.postSenderBasicChannelBasicEvent( 1 );
    helper.checkReceiverNotDelivered( TestHelper.ReceiverNumber.FIRST );
  }

  @Test
  public void unsubscribeNull() {
    // Test EventBus.unsubscribe() with null channel name
    // Must be no exceptions
    helper.getBus().unsubscribe( null );
  }

  @Test
  public void unsubscribeEmptyString() {
    // Test EventBus.unsubscribe() with empty string channel name
    // Must be no exceptions
    helper.getBus().unsubscribe( Constants.EMPTY_STRING );
  }

  @Test
  public void unsubscribeChannelSeveralTimes() {
    // Test EventBus.unsubscribe() several times invocation
    // Subscribe to BASIC_CHANNEL.
    // Unsubscribe from BASIC_CHANNEL for several times.
    // Check if event is not delivered.
    // Must be no exceptions.

    helper.subscribeReceiverBasicChannelBasicEvent( TestHelper.ReceiverNumber.FIRST );

    for (int i = 0; i < Constants.UNSUBSCRIBE_COUNT; i++) {
      helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).unsubscribe( Constants.BASIC_CHANNEL );
    }

    helper.postSenderBasicChannelBasicEvent( 1 );
    helper.checkReceiverNotDelivered( TestHelper.ReceiverNumber.FIRST );
  }

  @Test
  public void unsubscribeDifferentChannel() {
    // Test that calling EventBus.unsubscribe() for one channel
    // does not affect subscriptions on other channels.

    // Subscribe receiver to DEFAULT channel
    helper.subscribeReceiverBasicEvent( TestHelper.ReceiverNumber.FIRST );
    // Subscribe receiver2 to BASIC_CHANNEL
    helper.subscribeReceiverBasicChannelBasicEvent( TestHelper.ReceiverNumber.SECOND );

    // Send events to both channels and check delivery.
    // Both events MUST be delivered.
    helper.postEvents();
    helper.checkReceiverDelivered( TestHelper.ReceiverNumber.FIRST );
    helper.checkReceiverDelivered( TestHelper.ReceiverNumber.SECOND );

    // Reset event delivered flags
    helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).setEventDelivered( false );
    helper.getReceiver( TestHelper.ReceiverNumber.SECOND ).setEventDelivered( false );

    // Unsubscribe first receiver from DEFAULT channel
    helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).unsubscribe();

    // Send events again and check if first event is NOT delivered and second event IS delivered
    helper.postEvents();
    helper.checkReceiverNotDelivered( TestHelper.ReceiverNumber.FIRST );
    helper.checkReceiverDelivered( TestHelper.ReceiverNumber.SECOND );
  }

  @Test
  public void multipleReceiverChannelDelivery() {
    // Two receivers subscribe on BASIC_EVENT on BASIC_CHANNEL.
    // Event must be delivered to both receivers.

    helper.subscribeReceiverBasicChannelBasicEvent( TestHelper.ReceiverNumber.FIRST );
    helper.subscribeReceiverBasicChannelBasicEvent( TestHelper.ReceiverNumber.SECOND );
    helper.postSenderBasicChannelBasicEvent( 1 );
    helper.checkReceiverDelivered( TestHelper.ReceiverNumber.FIRST );
    helper.checkReceiverDelivered( TestHelper.ReceiverNumber.SECOND );
  }

  @Test
  public void differentChannelDelivery() {
    // Test equal event name delivery on different channels.
    // Event must be delivered only on that channel, to which it is posted.

    // First receiver subscribes on DEFAULT channel
    helper.subscribeReceiverBasicEvent( TestHelper.ReceiverNumber.FIRST );
    // Second receiver subscribes on BASIC_CHANNEL channel
    // Event name is the same in both channels
    helper.subscribeReceiverBasicChannelBasicEvent( TestHelper.ReceiverNumber.SECOND );

    // Send BASIC_EVENT to DEFAULT channel, first receiver MUST receive event, second receiver - must NOT
    helper.postSenderBasicEvent( 1 );
    helper.checkReceiverDelivered( TestHelper.ReceiverNumber.FIRST );
    helper.checkReceiverNotDelivered( TestHelper.ReceiverNumber.SECOND );
  }

  @Test
  public void senderReceiverMultipleConsumersWithThreadSleep() {
    // Test subscriber method invoke sequence.
    // SubscribeComplex() method subscribes 3 tasks to BASIC_EVENT.
    // SubscribeComplex() method is called SUBSCRIBE_COMPLEX_COUNT times.
    // The result task list must contain SUBSCRIBE_COMPLEX_COUNT * 3 tasks.
    // After event is posted, tasks are executed in order they have been subscribed.
    // Next task is executed only after previous task is done.

    for (int i = 0; i < Constants.SUBSCRIBE_COMPLEX_COUNT; i++) {
      helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).subscribeComplex( Constants.BASIC_EVENT );
    }

    helper.postSenderBasicEvent( 1 );

    // Check if first counters inside receiver have been incremented SUBSCRIBE_COMPLEX_COUNT times
    assertEquals( Constants.SUBSCRIBE_COMPLEX_COUNT * Constants.FIRST_COUNTER_INCREMENT, helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).getCounter() );
    // Check if second counters inside receiver have been incremented SUBSCRIBE_COMPLEX_COUNT * SECOND_COUNTER_INCREMENT_COUNT times
    assertEquals( Constants.SUBSCRIBE_COMPLEX_COUNT * Constants.SECOND_COUNTER_INCREMENT_COUNT * Constants.SECOND_COUNTER_INCREMENT, helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).getCounter2() );
  }
}
