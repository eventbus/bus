import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

// Test exception in subscriber method.
// WARNING: Don't be confused by stacktrace output!
// All tests MUST pass and MUST generate stacktrace output.
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ExceptionTest {

  private TestHelper helper;

  @Before
  public void init() {
    helper = new TestHelper();
  }

  @Test
  public void exceptionSubscriber() {
    // Must be no exceptions in EventBus, only stacktrace output.
    // Event must not be delivered.
    
    helper.subscribeReceiverBasicEventDivisionByZero( TestHelper.ReceiverNumber.FIRST );
    helper.postSenderBasicEvent( 1 );
    helper.checkReceiverNotDelivered( TestHelper.ReceiverNumber.FIRST );
  }

  @Test
  public void exceptionMultipleSubscriber() {
    // Test that exception in one subscriber method does not affect other subscribers.

    // Init 3 subscribers on BASIC_EVENT on DEFAULT channel:
    // 1. Division by zero
    // 2. eventDelivered = true
    // 3. increment counter
    helper.subscribeReceiverBasicEventDivisionByZero( TestHelper.ReceiverNumber.FIRST );
    helper.subscribeReceiverBasicEvent( TestHelper.ReceiverNumber.FIRST );
    helper.subscribeReceiverIncrementCounter( TestHelper.ReceiverNumber.FIRST );

    // Send event
    helper.postSenderBasicEvent( 1 );

    // Check if second and third subscribers executed successfully
    helper.checkReceiverDelivered( TestHelper.ReceiverNumber.FIRST );
    helper.checkCounter( Constants.FIRST_COUNTER_INCREMENT );
  }

  @Test
  public void exceptionSubscriberOnce() {
    // Test EventBus.once() with exception in subscriber method.
    // Subscriber must be removed after first invocation.
    // Must be no exceptions in EventBus, only stacktrace output.

    helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).subscribeDivisionByZeroWithCounterOnce( Constants.BASIC_EVENT );
    helper.postAndCheckOnceAndMany( true );
  }

  @Test
  public void exceptionSubscriberMany() {
    // Test EventBus.many() with exception in subscriber method.
    // Subscriber must be removed after SUBSCRIBER_EVENT_COUNT invocations.
    // Must be no exceptions in EventBus, only stacktrace output.

    helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).subscribeDivisionByZeroWithCounterMany( Constants.BASIC_EVENT, Constants.SUBSCRIBER_EVENT_COUNT );
    helper.postAndCheckOnceAndMany( false );
  }
}
