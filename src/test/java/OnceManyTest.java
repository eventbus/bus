import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


// Test EventBus.once() and many() methods
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OnceManyTest {

  private TestHelper helper;

  @Before
  public void init() {
    helper = new TestHelper();
  }

  @Test
  public void senderReceiverDeliveryOnce() {
    // Test EventBus.once() with default channel
    // Send event several times.
    // Check if Receiver incremented counter on event delivery only once
    // and the corresponding task list is empty.

    helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).subscribeIncrementCounterOnce( Constants.BASIC_EVENT );
    helper.postAndCheckOnceAndMany( true );
  }

  @Test
  public void senderReceiverDeliveryOnceChannel() {
    // Test EventBus.once() with specified channel

    helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).subscribeIncrementCounterOnce( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT );
    helper.postAndCheckOnceAndManyBasicChannel( true );
  }

  @Test
  public void senderReceiverDeliveryMany() {
    // Test EventBus.many() with default
    // Receiver subscribes to react on the event SUBSCRIBER_EVENT_COUNT times.
    // Send event SEND_EVENT_COUNT times.
    // Check if Receiver incremented counter on event delivery SUBSCRIBER_EVENT_COUNT times only
    // and the corresponding task list is empty.

    helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).subscribeIncrementCounterMany( Constants.BASIC_EVENT, Constants.SUBSCRIBER_EVENT_COUNT );
    helper.postAndCheckOnceAndMany( false );
  }

  @Test
  public void senderReceiverDeliveryManyChannel() {
    // Test EventBus.many() with specified channel

    helper.getReceiver( TestHelper.ReceiverNumber.FIRST ).subscribeIncrementCounterMany( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT, Constants.SUBSCRIBER_EVENT_COUNT );
    helper.postAndCheckOnceAndManyBasicChannel( false );
  }

  @Test
  public void nullSubscriberOnce() {
    // Test EventBus.once() with null consumer parameter.
    // Must be no exceptions and no event delivery.

    helper.getBus().once( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT, null );
    helper.postAndCheckNotDelivered();
  }

  @Test
  public void nullSubscriberMany() {
    // Test EventBus.many() with null consumer parameter.
    // Must be no exceptions and no event delivery.

    helper.getBus().many( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT, Constants.SUBSCRIBER_EVENT_COUNT, null );
    helper.postAndCheckNotDelivered();
  }

  @Test
  public void nullCountMany() {
    // Test EventBus.many() with null count parameter.
    // Must be no exceptions and no event delivery.

    helper.verifyCountManyNotDelivered( null );
  }

  @Test
  public void negativeCountMany() {
    // Test EventBus.many() with negative count parameter.
    // Must be no exceptions and no event delivery.

    helper.verifyCountManyNotDelivered( -1 );
  }

  @Test
  public void zeroCountMany() {
    // Test EventBus.many() with zero count parameter.
    // Must be no exceptions and no event delivery.

    helper.verifyCountManyNotDelivered( 0 );
  }
}
