import com.bitbucket.eventbus.mqtt.MqttPlugin;
import org.fusesource.hawtbuf.Buffer;
import org.fusesource.mqtt.client.Promise;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.assertEquals;


// Test EventBus.on() and post() methods with default channel only
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PluginsTest {

  private TestHelper helper;
  private MqttPlugin mqtt_plugin;

  @Before
  public void init() {
    helper = new TestHelper();
    mqtt_plugin = new MqttPlugin("metimur.ru", 1883, "mqtt");
  }

  @Test
  public void shoudSendAndRecieveHello() throws Exception {

    Promise<Buffer> result = new Promise<Buffer>();

    helper
      .getBus()
      .withPlugin(
        mqtt_plugin
          .withClientId("mqtt_test_clientId")
          .withUsername("mqtt_test_user")
          .withPassword("mqtt_test_password")
          .withDebug()
          .withResult(result)
          .build()
      );

    assertEquals("Hello", result.await().utf8().toString() );
  }

}
