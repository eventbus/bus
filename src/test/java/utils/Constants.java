class Constants {
  static final String BASIC_CHANNEL = "basicChannel";
  static final String EMPTY_STRING  = "";
  static final String EMPTY_EVENT   = "emptyEvent";
  static final String BASIC_EVENT   = "basicEvent";
  static final String DATA_EVENT    = "dataEvent";
  static final String STRING_DATA   = "StringData";

  static final Integer INTEGER_DATA               = 15;
  static final int EVENT_QUANTITY                 = 10000000; // Number of events sent in benchmarks and multiple event tests
  static final int EVENT_WITH_DATA_QUANTITY       = 1000;     // Number of events sent in multiple event with data tests
  static final int DUMMY_WORK_DELAY               = 1000;     // in milliseconds
  static final int SUBSCRIBER_EVENT_COUNT         = 3;        // Count value for EventBus.many() method
  static final int SEND_EVENT_COUNT               = 5;        // Number of events to send
  static final int FIRST_COUNTER_INCREMENT        = 1;        // Receiver's first counter is incremented by this value
  static final int SECOND_COUNTER_INCREMENT       = 2;        // Receiver's second counter is incremented by this value
  static final int SECOND_COUNTER_INCREMENT_COUNT = 2;        // Number of times the second counter is incremented
  static final int SUBSCRIBE_COMPLEX_COUNT        = 2;        // Number of times Receiver.subscribeComplex() method is called
  static final int UNSUBSCRIBE_COUNT              = 5;        // Number of times Receiver.unsubscribe() is called
  static final int SUBSCRIBE_BACKGROUND_COUNT     = 5;        // Number of subscribers in background mode
  static final int SUBSCRIBE_MAIN_COUNT           = 1;        // Number of subscribers in main mode
}
