import com.bitbucket.eventbus.EventBus;

import java.util.concurrent.ExecutorService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


// Contains fields and methods used in different tests
class TestHelper {

  // Number of the receiver to be used
  public enum ReceiverNumber {
    FIRST,
    SECOND
  }

  private EventBus bus;
  private Sender sender;
  private Receiver receiver;
  private Receiver receiver2;

  private boolean eventDelivered;
  private String deliveredString;
  private Integer deliveredInteger;

  TestHelper() {
    init();
  }

  private void init() {
    EventBus.init();
    bus = EventBus.getInstance();
    sender = new Sender( bus );
    receiver = new Receiver( bus );
    receiver2 = new Receiver( bus );
    eventDelivered = false;
  }

  EventBus getBus() {
    return bus;
  }

  void setEventDelivered() {
    this.eventDelivered = true;
  }

  String getDeliveredString() {
    return deliveredString;
  }

  Integer getDeliveredInteger() {
    return deliveredInteger;
  }

  Sender getSender() {
    return sender;
  }

  // Return receiver or receiver2 depending on receiverNumber value
  Receiver getReceiver(ReceiverNumber receiverNumber) {
    Receiver result;

    switch (receiverNumber) {
      case FIRST:
        result = receiver;
        break;

      case SECOND:
        result = receiver2;
        break;

      default:
        result = receiver;
    }

    return result;
  }

  // === HELPER METHODS ===
  // --- Subscribe ---

  // Subscribe to BASIC_EVENT on DEFAULT channel
  void subscribeBasicEvent() {
    bus.on( Constants.BASIC_EVENT, this::onBasicEvent );
  }

  // Subscribe Receiver to BASIC_EVENT on DEFAULT channel
  void subscribeReceiverBasicEvent(ReceiverNumber receiverNumber) {
    getReceiver( receiverNumber ).subscribe( Constants.BASIC_EVENT );
  }

  // Subscribe Receiver to BASIC_EVENT on BASIC_CHANNEL
  void subscribeReceiverBasicChannelBasicEvent(ReceiverNumber receiverNumber) {
    getReceiver( receiverNumber ).subscribe( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT );
  }

  // Subscriber receiver's division by zero method to BASIC_EVENT on DEFAULT channel
  void subscribeReceiverBasicEventDivisionByZero(ReceiverNumber receiverNumber) {
    getReceiver( receiverNumber ).subscribeDivisionByZero( Constants.BASIC_EVENT );
  }

  // Subscriber receiver's increment counter method to BASIC_EVENT on DEFAULT channel
  void subscribeReceiverIncrementCounter(ReceiverNumber receiverNumber) {
    getReceiver( receiverNumber ).subscribeIncrementCounter( Constants.BASIC_EVENT );
  }

  // Subscriber receiver's increment counter method to BASIC_EVENT on BASIC_CHANNEL in background mode
  void subscribeReceiverIncrementCounterBasicChannelBackground(ReceiverNumber receiverNumber) {
    getReceiver( receiverNumber ).subscribeIncrementCounterBackground( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT );
  }

  // --- Post ---

  // Send BASIC_EVENT to DEFAULT channel
  void postBasicEvent() {
    bus.post( Constants.BASIC_EVENT, Constants.STRING_DATA );
  }

  // Send BASIC_EVENT to DEFAULT channel from Sender
  void postSenderBasicEvent(int count) {
    for (int i = 0; i < count; i++) {
      sender.send( Constants.BASIC_EVENT, Constants.STRING_DATA );
    }
  }

  // Send BASIC_EVENT to BASIC_CHANNEL for count times
  void postBasicChannelBasicEvent(int count) {
    for (int i = 0; i < count; i++) {
      bus.post( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT, Constants.STRING_DATA );
    }
  }

  // Send BASIC_EVENT to BASIC_CHANNEL from Sender for count times
  void postSenderBasicChannelBasicEvent(int count) {
    for (int i = 0; i < count; i++) {
      sender.send( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT, Constants.STRING_DATA );
    }
  }

  // Send BASIC_EVENT to DEFAULT channel and BASIC_CHANNEL
  void postEvents() {
    postSenderBasicEvent( 1 );
    postSenderBasicChannelBasicEvent( 1 );
  }

  // --- Check ---

  // Check if event has been delivered
  void checkDelivered() {
    assertTrue( eventDelivered );
  }

  // Check if event has been delivered in background mode
  void checkDeliveredSlow() {
    waitBackground();
    checkDelivered();
  }

  // Check if event has NOT been delivered
  void checkNotDelivered() {
    assertFalse( eventDelivered );
  }

  // Check if event has been delivered to Receiver
  void checkReceiverDelivered(ReceiverNumber receiverNumber) {
    assertTrue( getReceiver( receiverNumber ).isEventDelivered() );
  }

  // Check if event has NOT been delivered to Receiver
  void checkReceiverNotDelivered(ReceiverNumber receiverNumber) {
    assertFalse( getReceiver( receiverNumber ).isEventDelivered() );
  }

  // Check if task list for BASIC_EVENT on specified channel is empty
  private void checkTaskListEmpty(String channel) {
    int taskCount = bus.getChannels().get( channel ).getEvents().get( Constants.BASIC_EVENT ).size();
    assertEquals( 0, taskCount );
  }

  // Check Receiver counter value
  void checkCounter(int expected) {
    assertEquals( expected, receiver.getCounter() );
  }

  // Check Receiver counter value after event delivery in background mode
  void checkCounterSlow(int expected) {
    waitBackground();
    checkCounter( expected );
  }

  // --- Post and check ---

  // Send BASIC_EVENT on BASIC_CHANNEL for SEND_EVENT_COUNT times and check if event has not been delivered
  void postAndCheckNotDelivered() {
    postBasicChannelBasicEvent( Constants.SEND_EVENT_COUNT );
    checkNotDelivered();
  }

  // Send BASIC_EVENT on DEFAULT channel for SEND_EVENT_COUNT times,
  // check receiver counter and check if subscriber has been removed.
  void postAndCheckOnceAndMany(boolean once) {
    postAndCheckOnceAndMany( once, true, false );
  }

  // Send BASIC_EVENT on BASIC_CHANNEL for SEND_EVENT_COUNT times,
  // check receiver counter and check if subscriber has been removed.
  void postAndCheckOnceAndManyBasicChannel(boolean once) {
    postAndCheckOnceAndMany( once, false, false );
  }

  void postAndCheckOnceAndManySlow(boolean once) {
    postAndCheckOnceAndMany( once, false, true );
  }

  // Verify EventBus.once() or many() methods with BASIC_EVENT
  // on DEFAULT channel or BASIC_CHANNEL on main or background threads.
  private void postAndCheckOnceAndMany(boolean once, boolean defaultChannel, boolean background) {
    if ( defaultChannel ) {
      postSenderBasicEvent( Constants.SEND_EVENT_COUNT );
    } else {
      postSenderBasicChannelBasicEvent( Constants.SEND_EVENT_COUNT );
    }

    if ( background ) {
      waitBackground();
    }

    if ( once ) {
      checkCounter( Constants.FIRST_COUNTER_INCREMENT );
    } else {
      checkCounter( Constants.SUBSCRIBER_EVENT_COUNT * Constants.FIRST_COUNTER_INCREMENT );
    }

    if ( defaultChannel ) {
      checkTaskListEmpty( EventBus.DEFAULT_CHANNEL );
    } else {
      checkTaskListEmpty( Constants.BASIC_CHANNEL );
    }
  }

  // Check if event for many() subscription with provided count parameter has not been delivered
  void verifyCountManyNotDelivered(Integer count) {
    bus.many( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT, count, this::onBasicEvent );
    postAndCheckNotDelivered();
  }

  // Verify multiple subscribers in background mode with or without main mode depending on withMain value
  void verifyMultipleSubscriberBackground(boolean withMain) {
    for (int i = 0; i < Constants.SUBSCRIBE_BACKGROUND_COUNT; i++) {
      subscribeReceiverIncrementCounterBasicChannelBackground( ReceiverNumber.FIRST );
    }

    if ( withMain ) {
      for (int i = 0; i < Constants.SUBSCRIBE_MAIN_COUNT; i++) {
        receiver.subscribeIncrementCounter( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT );
      }
    }

    postSenderBasicChannelBasicEvent( 1 );

    int subscriberCount = withMain ? Constants.SUBSCRIBE_BACKGROUND_COUNT + Constants.SUBSCRIBE_MAIN_COUNT : Constants.SUBSCRIBE_BACKGROUND_COUNT;

    checkCounterSlow( subscriberCount * Constants.FIRST_COUNTER_INCREMENT );
  }

  // Verify multiple event with different data delivery in current or background mode
  void verifyMultipleEventWithDataDelivery(boolean background) {
    getReceiver( TestHelper.ReceiverNumber.FIRST ).subscribeIncrementCounterByValue( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT, background );

    int sum = 0;

    for (int i = 0; i < Constants.EVENT_WITH_DATA_QUANTITY; i++) {
      // Send current i value with every event
      bus.post( Constants.BASIC_CHANNEL, Constants.BASIC_EVENT, i );
      // Calculate expected value
      sum += i;
    }

    // Check if receiver's counter value is equal to sum value
    if ( background ) {
      checkCounterSlow( sum );
    } else {
      checkCounter( sum );
    }
  }

  // --- Misc ---

  // Wait for background threads to finish
  private void waitBackground() {
    // Initialize event bus thread pool shutdown
    ExecutorService pool = bus.getExecutor().getBackgroundExecutor().getPool();
    pool.shutdown();

    // Wait for background tasks to complete
    while ( !pool.isTerminated() ) {
    }
  }

  // === SUBSCRIBER METHODS ===
  // These methods are invoked by EventBus on event delivery

  void onBasicEvent(Object data) {
    eventDelivered = true;
  }

  void onStringEvent(Object data) {
    eventDelivered = true;

    if ( data instanceof String ) {
      deliveredString = (String) data;
    }
  }

  void onIntegerEvent(Object data) {
    eventDelivered = true;

    if ( data instanceof Integer ) {
      deliveredInteger = (Integer) data;
    }
  }
}
