import com.bitbucket.eventbus.EventBus;
import com.bitbucket.eventbus.core.enums.Mode;

// Subscribes to EventBus and receives events
class Receiver {

  private EventBus bus;
  private boolean eventDelivered;
  private int counter;
  private int counter2;

  Receiver(EventBus bus) {
    this.bus = bus;
    setEventDelivered( false );
    this.counter = 0;
    this.counter2 = 0;
  }

  boolean isEventDelivered() {
    return eventDelivered;
  }

  void setEventDelivered(boolean eventDelivered) {
    this.eventDelivered = eventDelivered;
  }

  int getCounter() {
    return counter;
  }

  int getCounter2() {
    return counter2;
  }

  // Subscribe to default channel
  // Set eventDelivered to true on event received
  void subscribe(String eventName) {
    bus.on( eventName, object -> setEventDelivered( true ) );
  }

  // Subscribe to specific channel
  void subscribe(String channelName, String eventName) {
    bus.on( channelName, eventName, object -> setEventDelivered( true ) );
  }

  // Do nothing on event received
  void subscribeEmpty(String eventName) {
    bus.on( eventName, this::emptyMethod );
  }

  // Subscribe several subscribers to eventName event on default channel
  void subscribeComplex(String eventName) {
    bus.on( eventName, object -> {
      System.out.println("First counter increment");
      incrementFirstCounter( null );
      try {
        Thread.sleep( Constants.DUMMY_WORK_DELAY );
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    });

    for (int i = 0; i < Constants.SECOND_COUNTER_INCREMENT_COUNT; i++) {
      bus.on( eventName, object -> {
        System.out.println("Second counter increment");
        counter2 = counter2 + Constants.SECOND_COUNTER_INCREMENT;
      });
    }
  }

  // Subscribe incrementFirstCounter() method to run once on eventName event delivery on default channel
  void subscribeIncrementCounterOnce(String eventName) {
    bus.once( eventName, this::incrementFirstCounter );
  }

  // Subscribe incrementFirstCounter() method to run once on eventName event delivery on specified channel
  void subscribeIncrementCounterOnce(String channelName, String eventName) {
    bus.once( channelName, eventName, this::incrementFirstCounter );
  }

  // Subscribe incrementFirstCounter() method to run once on eventName event delivery on specified channel in background mode
  void subscribeIncrementCounterOnceBackground(String channelName, String eventName) {
    bus.once( channelName, eventName, this::incrementFirstCounter, Mode.BACKGROUND );
  }

  // Subscribe incrementFirstCounter() method to run count times on eventName event delivery on default channel
  void subscribeIncrementCounterMany(String eventName, int count) {
    bus.many( eventName, count, this::incrementFirstCounter );
  }

  // Subscribe incrementFirstCounter() method to run count times on eventName event delivery on specified channel
  void subscribeIncrementCounterMany(String channelName, String eventName, int count) {
    bus.many( channelName, eventName, count, this::incrementFirstCounter );
  }

  // Subscribe incrementFirstCounter() method to run count times on eventName event delivery on specified channel in background mode
  void subscribeIncrementCounterManyBackground(String channelName, String eventName, int count) {
    bus.many( channelName, eventName, count, this::incrementFirstCounter, Mode.BACKGROUND );
  }

  // Subscribe incrementFirstCounter() method to run on eventName event delivery on default channel
  void subscribeIncrementCounter(String eventName) {
    bus.on( eventName, this::incrementFirstCounter );
  }

  // Subscribe incrementFirstCounter() method to run on eventName event delivery on specified channel
  void subscribeIncrementCounter(String channelName, String eventName) {
    bus.on( channelName, eventName, this::incrementFirstCounter );
  }

  // Subscribe incrementFirstCounterByValue() method to run on eventName event delivery on specified channel in current or background mode
  void subscribeIncrementCounterByValue(String channelName, String eventName, boolean background) {
    bus.on( channelName, eventName, this::incrementFirstCounterByValue, background ? Mode.BACKGROUND : Mode.CURRENT );
  }

  // Subscribe incrementFirstCounter() method to run on eventName event delivery on specified channel in background mode
  void subscribeIncrementCounterBackground(String channelName, String eventName) {
    bus.on( channelName, eventName, this::incrementFirstCounter, Mode.BACKGROUND );
  }

  // Subscribe to eventName on DEFAULT channel. Subscriber method throws division by zero exception.
  void subscribeDivisionByZero(String eventName) {
    bus.on( eventName, this::divisionByZero );
  }

  // Subscribe divisionByZeroWithCounter() method to run once on eventName event delivery on default channel
  void subscribeDivisionByZeroWithCounterOnce(String eventName) {
    bus.once( eventName, this::divisionByZeroWithCounter );
  }

  // Subscribe divisionByZeroWithCounter() method to run count times on eventName event delivery on default channel
  void subscribeDivisionByZeroWithCounterMany(String eventName, int count) {
    bus.many( eventName, count, this::divisionByZeroWithCounter );
  }

  // Unsubscribe from default channel
  void unsubscribe() {
    bus.unsubscribe( EventBus.DEFAULT_CHANNEL );
  }

  // Unsubscribe from specified channel
  void unsubscribe(String channelName) {
    bus.unsubscribe( channelName );
  }

  // Do nothing
  private void emptyMethod(Object data) {
  }

  // Increment first counter by FIRST_COUNTER_INCREMENT value.
  // Method is synchronized for proper counter calculation in background mode if multiple events are delivered.
  private synchronized void incrementFirstCounter(Object data) {
    counter = counter + Constants.FIRST_COUNTER_INCREMENT;
  }

  // Increment first counter by data value.
  private synchronized void incrementFirstCounterByValue(Object data) {
    if ( data != null && data instanceof Integer ) {
      counter = counter + (Integer) data;
    }
  }

  // Throw division by zero exception
  private void divisionByZero(Object data) {
    int x = 1 / 0;
  }

  // Increment first counter and throw division by zero exception
  private void divisionByZeroWithCounter(Object data) {
    incrementFirstCounter( data );
    divisionByZero( data );
  }
}
