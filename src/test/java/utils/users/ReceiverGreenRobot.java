import org.greenrobot.eventbus.*;

// Subscribes to GreenRobot EventBus and receives events
public class ReceiverGreenRobot {

  void register() {
    org.greenrobot.eventbus.EventBus.getDefault().register( this );
  }

  void unregister() {
    org.greenrobot.eventbus.EventBus.getDefault().unregister( this );
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void onMessageEvent(EmptyEvent event) {
    // Do nothing
  }
}
