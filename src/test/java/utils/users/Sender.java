import com.bitbucket.eventbus.EventBus;

// Sends events to EventBus
class Sender {

  private EventBus bus;

  Sender(EventBus bus) {
    this.bus = bus;
  }

  // Send event to default channel
  void send(String eventName, String data) {
    bus.post( eventName, data );
  }

  // Send event to specific channel
  void send(String channelName, String eventName, String data) {
    bus.post( channelName, eventName, data );
  }
}
