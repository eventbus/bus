// Sends events to GreenRobot EventBus
class SenderGreenRobot {

  void sendEmpty() {
    org.greenrobot.eventbus.EventBus.getDefault().post( new EmptyEvent() );
  }
}
